# Reverse Polish Notation - Kotlin

Coding Dojo - Reverse Polish Notation - https://codingdojo.org/kata/RPN/

## How to run it

You need docker (nothing more)

### Compile, test and create docker image

Run the following command from the project root folder:

`docker build -f Dockerfile -t rpn .`

and then:

`docker run --rm -it rpn "$INPUT"`

Examples:

```
docker run --rm -it rpn:latest "5 3 +"
docker run --rm -it rpn:latest "4 5 MAX 1 2 MAX * 9 SQRT +"
```

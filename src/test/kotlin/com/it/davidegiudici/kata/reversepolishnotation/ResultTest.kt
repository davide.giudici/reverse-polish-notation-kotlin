package com.it.davidegiudici.kata.reversepolishnotation

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs

class ResultTest {

    @Test
    fun `flatten a list with also errors, return the first error`() {
        val errorAndOkResults = listOf(
            Result.Ok(5), Result.Ok(6),
            Result.Fail("1"), Result.Ok(1),
            Result.Fail("2")
        )

        val flattenResult = errorAndOkResults.flatten()
        assertIs<Result.Fail<String>>(flattenResult)
        assertEquals("1", flattenResult.reason)
    }

    @Test
    fun `flatten a list without any errors, return the list as result ok`() {
        val onlyOkResultList = listOf(
            Result.Ok(5), Result.Ok(6), Result.Ok(1),
        )

        val flattenResult = onlyOkResultList.flatten()
        assertIs<Result.Ok<List<String>>>(flattenResult)
        assertEquals(onlyOkResultList.size, flattenResult.result.size)
    }

    @Test
    fun `flatMap-ping an error result, return the error`() {
        val reason = "error1"
        val error: Result<Int, String> = Result.Fail(reason)

        val mapResult = error.flatMap {
            Result.Ok(it.toLong())
        }

        assertIs<Result.Fail<String>>(mapResult)
        assertEquals(reason, mapResult.reason)
    }

    @Test
    fun `flatMap-ping an ok result, return the mapped result`() {
        val value = 145
        val error: Result<Int, String> = Result.Ok(value)

        val mapResult = error.flatMap {
            Result.Ok(it.toLong())
        }

        assertIs<Result.Ok<Long>>(mapResult)
        assertEquals(value.toLong(), mapResult.result)
    }
}

package com.it.davidegiudici.kata.reversepolishnotation

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class TokenTest {

    @TestFactory
    fun `creating a token`() =
        listOf(
            "5" to Token.Number::class.java.simpleName,
            "+" to Token.Binary.Plus::class.java.simpleName,
            "-" to Token.Binary.Minus::class.java.simpleName,
            "*" to Token.Binary.Multiply::class.java.simpleName,
            "/" to Token.Binary.Division::class.java.simpleName,
            "sqrt" to Token.Unary.SquareRoot::class.java.simpleName,
            "max" to Token.Nnary.Max::class.java.simpleName,
        ).map { (input, type) ->
            DynamicTest.dynamicTest("given the string $input it should become a token of type $type") {

                val result = tokenBuilder(input)
                assertNotNull(result)
                assertIs<Result.Ok<Expression>>(result)
                assertEquals(type, result.result::class.simpleName)
            }
        }

    @TestFactory
    fun `string that does not creating a tokens return null`() =
        listOf("5.0", "something", "+*").map { input ->
            DynamicTest.dynamicTest("given the string $input it should become a NULL token") {
                val result = tokenBuilder(input)
                assertTrue(result is Result.Fail)
            }
        }
}

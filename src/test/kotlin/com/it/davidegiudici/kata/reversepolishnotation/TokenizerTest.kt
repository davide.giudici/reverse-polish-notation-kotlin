package com.it.davidegiudici.kata.reversepolishnotation

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class TokenizerTest {
    @Test
    fun `tokenize a string split by spaces`() {
        val tokens = tokenize("a b 1 123 fsdf")

        assertEquals(5, tokens.size)
        assertEquals("123", tokens[3])
    }
}
package com.it.davidegiudici.kata.reversepolishnotation

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertIs

typealias Error = Result.Fail<Errors>

class ExpressionTest {

    private val exp = ExpressionBuilder()

    @Test
    fun `binary operator after only one numbers results in a missing number error`() {
        val tokens = // 5 +
            listOf(
                Token.Number(5),
                Token.Binary.Plus
            )
        val reason = assertIs<Error>(exp.build(tokens))
        assertEquals(Errors.EXPECTED_NUMBER, reason.reason)
    }

    @Test
    fun `binary operator after three numbers results in a missing operator error`() {
        val tokens = // 5 4 3 +
            listOf(
                Token.Number(5),
                Token.Number(4),
                Token.Number(3),
                Token.Binary.Plus,
            )
        val reason = assertIs<Error>(exp.build(tokens))
        assertEquals(Errors.EXPECTED_OPERATOR, reason.reason)
    }

    @Test
    fun `unary operator followed by a number results in a valid expression`() {
        val tokens = // SQRT 9
            listOf(
                Token.Unary.SquareRoot,
                Token.Number(9),
            )
        val reason = assertIs<Error>(exp.build(tokens))
        assertEquals(Errors.EXPECTED_NUMBER, reason.reason)
    }

    @Test
    fun `unary operator tha follow a number results in a invalid expression`() {
        val tokens = // 9 SQRT
            listOf(
                Token.Number(9),
                Token.Unary.SquareRoot,
            )
        assertIs<Result.Ok<Expression>>(exp.build(tokens))
    }

    @Test
    fun `binary operator after two numbers results in a valid expression`() {
        val tokens = // 5 6 +
            listOf(
                Token.Number(5),
                Token.Number(6),
                Token.Binary.Plus,
            )
        assertIs<Result.Ok<Expression>>(exp.build(tokens))
    }

    @Test
    fun `two binary operator after fours numbers results in a valid expression`() {
        val tokens = // 5 4 6 + -
            listOf(
                Token.Number(5),
                Token.Number(4),
                Token.Number(6),
                Token.Binary.Plus,
                Token.Binary.Minus,
            )
        assertIs<Result.Ok<Expression>>(exp.build(tokens))
    }

    @TestFactory
    fun `max operator after any amount of numbers results in a valid expression`() =
        listOf(
            1, 3, 10
        ).map { amountOfNumber ->
            DynamicTest.dynamicTest("max operator after $amountOfNumber numbers results in a valid expression") {

                val tokens: MutableList<Token> = IntArray(amountOfNumber)
                { Random.nextInt(0, 20) }.map { Token.Number(it) }.toMutableList()
                tokens.add(Token.Nnary.Max)

                val result = exp.build(tokens)
                assertIs<Result.Ok<Expression>>(result)
                println(result.result)
            }
        }

    @Test
    fun `max operator after an binary expression results in a invalid expression`() {
        val tokens = // 5 3 + 4 MAX
            listOf(
                Token.Number(5),
                Token.Number(3),
                Token.Binary.Plus,
                Token.Number(4),
                Token.Nnary.Max
            )
        assertIs<Result.Ok<Expression>>(exp.build(tokens))
    }

    @Test
    fun `evaluating a number expression return its value`() {
        val exp = Expression.Number(15)
        assertEquals(15, exp.evaluate())
    }

    @TestFactory
    fun `evaluating binary operators return the correct result`() =
        listOf(
            Triple(5, 2, Token.Binary.Plus) to 7,
            Triple(2, 6, Token.Binary.Minus) to -4,
            Triple(3, 11, Token.Binary.Multiply) to 33,
            Triple(81, 5, Token.Binary.Division) to 16,
        ).map { (input, output) ->
            DynamicTest.dynamicTest("evaluating ${input.first} ${input.third} ${input.second} the result is $output") {
                val f = Expression.Number(input.first)
                val s = Expression.Number(input.second)
                val exp = Expression.Binary(f, s, input.third)

                assertEquals(output, exp.evaluate())
            }
        }

    @TestFactory
    fun `evaluating unary operators return the correct result`() =
        listOf(
            Pair(16, Token.Unary.SquareRoot) to 4,
            Pair(25, Token.Unary.SquareRoot) to 5,
            Pair(30, Token.Unary.SquareRoot) to 5,
            Pair(35, Token.Unary.SquareRoot) to 6,
        ).map { (input, output) ->
            DynamicTest.dynamicTest("evaluating ${input.second}(${input.first}) the result is $output") {
                val f = Expression.Number(input.first)
                val exp = Expression.Unary(f, input.second)

                assertEquals(output, exp.evaluate())
            }
        }

    @TestFactory
    fun `evaluating nnary operators return the correct result`() =
        listOf(
            Pair(listOf(16, 20), Token.Nnary.Max) to 20,
            Pair(listOf(-1, -5, -3), Token.Nnary.Max) to -1,
        ).map { (input, output) ->
            DynamicTest.dynamicTest("evaluating ${input.second}(${input.first.joinToString(",")}) the result is $output") {
                val args = input.first.map { Expression.Number(it) }
                val exp = Expression.Nnary(args, input.second)

                assertEquals(output, exp.evaluate())
            }
        }
}

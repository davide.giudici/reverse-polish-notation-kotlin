package com.it.davidegiudici.kata.reversepolishnotation

import org.junit.jupiter.api.*
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertIs

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AcceptanceTest {

    @TestFactory
    fun `evaluating a complex string operator return the right result`() =
        listOf(
            "20 5 /" to 4,
            "4 2 + 3 -" to 3,
            "3 5 8 * 7 + *" to 141,
            "3 5 8 + +" to 16,
            "9 SQRT" to 3,
            "4 5 9 SQRT + -" to -4,
            "5 3 4 2 9 1 MAX" to 9,
            "4 5 MAX 1 2 MAX *" to 10,
        ).map { (input, output) ->
            DynamicTest.dynamicTest("evaluating $input the result should be $output") {
                val result = expBuilderFacade(input)
                val castedResult = assertIs<Result.Ok<Expression>>(result)
                assertEquals(output, castedResult.result.evaluate())
            }
        }

    @Test
    fun `call main with right syntax input does not throws exception`() {
        val stdOut = collectStdOut {
            main(arrayOf("5 6 +"))
        }
        println(stdOut)
        assertContains(stdOut, "(5 + 6)")
        assertContains(stdOut, "Result: 11")
    }

    @Test
    fun `call main with wrong syntax input does not throws exception`() {
        assertDoesNotThrow {
            main(arrayOf("5 6 a"))
        }
    }

    @Test
    fun `call main without args does not throws exception`() {
        assertDoesNotThrow {
            main(arrayOf())
        }
    }

    private fun collectStdOut(f: () -> Unit) =
        ByteArrayOutputStream().use {
            System.setOut(PrintStream(it))
            f()
            System.setOut(System.out)

            it.toString()
        }
}

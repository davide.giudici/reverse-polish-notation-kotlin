package com.it.davidegiudici.kata.reversepolishnotation

fun main(args: Array<String>) {
    args
        .firstOrNull()
        ?.let {
            when (val result = expBuilderFacade(it)) {
                is Result.Ok -> {
                    val exp = result.result
                    println("Parsed expression: $exp")
                    println("Result: ${exp.evaluate()}")
                }
                is Result.Fail -> println(result.reason.description)
            }
        }
        ?: println("Missing input expression")
}

package com.it.davidegiudici.kata.reversepolishnotation

enum class Errors(val description: String) {
    INVALID_TOKEN("Invalid token found"),
    EXPECTED_OPERATOR("Expected operator in the expression"),
    EXPECTED_NUMBER("Expected number in the expression"),
    UNKNOWN("Unknown parsing error")
}
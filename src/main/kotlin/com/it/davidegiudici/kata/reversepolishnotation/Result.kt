package com.it.davidegiudici.kata.reversepolishnotation

sealed class Result<out V, out E> {
    class Ok<out V>(val result: V) : Result<V, Nothing>()
    class Fail<out E>(val reason: E) : Result<Nothing, E>()
}

inline fun <V, E, R> Result<V, E>.flatMap(block: (V) -> Result<R, E>): Result<R, E> {
    return when (this) {
        is Result.Ok -> block(this.result)
        is Result.Fail -> Result.Fail(this.reason)
    }
}

fun <V, E> List<Result<V, E>>.flatten(): Result<List<V>, E> {
    val failed = this.getFirstErrorOrNull()
    return if (failed == null)
        Result.Ok(this.filterOk().map { it.result })
    else
        Result.Fail(failed.reason)
}

private fun <V, E> List<Result<V, E>>.getFirstErrorOrNull() =
    this.filterIsInstance<Result.Fail<E>>().firstOrNull()

private fun <V, E> List<Result<V, E>>.filterOk() =
    this.filterIsInstance<Result.Ok<V>>()

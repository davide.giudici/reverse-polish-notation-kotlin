package com.it.davidegiudici.kata.reversepolishnotation

fun expBuilderFacade(input: String) =
    tokenize(input)
        .map {
            tokenBuilder(it)
        }
        .flatten()
        .flatMap {
            ExpressionBuilder().build(it)
        }

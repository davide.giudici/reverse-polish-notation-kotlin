package com.it.davidegiudici.kata.reversepolishnotation

import kotlin.math.roundToInt
import kotlin.math.sqrt

sealed class Token {

    sealed class Binary : Token() {
        abstract fun apply(x: Expression, y: Expression): Expression

        fun getExpression(firstArg: Expression, secondArg: Expression) =
            Expression.Binary(firstArg, secondArg, this)

        object Plus : Binary() {
            override fun apply(x: Expression, y: Expression) = Expression.Number(x.evaluate() + y.evaluate())
            override fun toString() = "+"
        }

        object Minus : Binary() {
            override fun apply(x: Expression, y: Expression) = Expression.Number(x.evaluate() - y.evaluate())
            override fun toString() = "-"
        }

        object Multiply : Binary() {
            override fun apply(x: Expression, y: Expression) = Expression.Number(x.evaluate() * y.evaluate())
            override fun toString() = "*"
        }

        object Division : Binary() {
            override fun apply(x: Expression, y: Expression) = Expression.Number(x.evaluate() / y.evaluate())
            override fun toString() = "/"
        }
    }

    sealed class Unary : Token() {
        abstract fun apply(x: Expression): Expression

        fun getExpression(arg: Expression) =
            Expression.Unary(arg, this)

        object SquareRoot : Unary() {
            override fun apply(x: Expression) =
                Expression.Number(sqrt(x.evaluate().toDouble()).roundToInt())

            override fun toString() = "SQRT"
        }
    }

    sealed class Nnary : Token() {
        abstract fun apply(x: List<Expression>): Expression

        fun getExpression(arg: List<Expression>) =
            Expression.Nnary(arg, this)

        object Max : Nnary() {
            override fun apply(x: List<Expression>) =
                Expression.Number(x.maxOf { it.evaluate() })

            override fun toString() = "MAX"
        }
    }

    class Number(private val value: Int) : Token() {
        fun getExpression() = Expression.Number(value)
    }
}

fun tokenize(input: String) = input.split(" ")

fun tokenBuilder(input: String): Result<Token, Errors> {
    return when {
        input == "+" -> Result.Ok(Token.Binary.Plus)
        input == "-" -> Result.Ok(Token.Binary.Minus)
        input == "*" -> Result.Ok(Token.Binary.Multiply)
        input == "/" -> Result.Ok(Token.Binary.Division)
        input.equals("SQRT", true) -> Result.Ok(Token.Unary.SquareRoot)
        input.equals("MAX", true) -> Result.Ok(Token.Nnary.Max)
        input.toIntOrNull() != null -> Result.Ok(Token.Number(input.toIntOrNull()!!))
        else -> Result.Fail(Errors.INVALID_TOKEN)
    }
}

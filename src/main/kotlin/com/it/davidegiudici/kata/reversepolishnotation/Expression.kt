package com.it.davidegiudici.kata.reversepolishnotation

import java.util.*

typealias ExpStack = Stack<Expression>
typealias TokenList = List<Token>

sealed class Expression {
    abstract fun evaluate(): Int

    class Number(private val value: Int) : Expression() {
        override fun evaluate() = value
        override fun toString() = value.toString()
    }

    class Binary(
        private val firstArg: Expression,
        private val secondArg: Expression,
        private val binaryOperator: Token.Binary
    ) : Expression() {
        override fun evaluate() = binaryOperator.apply(firstArg, secondArg).evaluate()

        override fun toString() = "($firstArg $binaryOperator $secondArg)"
    }

    class Unary(
        private val arg: Expression,
        private val unaryOperator: Token.Unary
    ) : Expression() {
        override fun evaluate() = unaryOperator.apply(arg).evaluate()

        override fun toString() = "($unaryOperator($arg))"
    }

    class Nnary(
        private val args: List<Expression>,
        private val nnaryOperator: Token.Nnary,
    ) : Expression() {
        override fun evaluate() = nnaryOperator.apply(args).evaluate()

        override fun toString() = "($nnaryOperator(${args.joinToString(",")}))"
    }
}

class ExpressionBuilder {
    private val stack = ExpStack()

    fun build(tokens: TokenList): Result<Expression, Errors> {
        tokens.forEach { tk ->
            val exp = when (tk) {
                is Token.Number -> Result.Ok(tk.getExpression())
                is Token.Unary -> buildUnaryOperator(tk)
                is Token.Binary -> buildBinaryOperator(tk)
                is Token.Nnary -> buildNnaryOperator(tk)
            }

            when (exp) {
                is Result.Ok -> stack.push(exp.result)
                is Result.Fail -> return exp
            }
        }

        return buildResult(stack)
    }

    private fun buildNnaryOperator(nnaryOperator: Token.Nnary): Result<Expression, Errors> {
        if (stack.size < 1)
            return Result.Fail(Errors.EXPECTED_NUMBER)

        val args = mutableListOf<Expression>()
        while (!stack.isEmpty() && stack.peek() !is Expression.Nnary) {
            args.add(stack.pop())
        }

        val exp = nnaryOperator.getExpression(args)
        return Result.Ok(exp)
    }

    private fun buildUnaryOperator(unaryOperator: Token.Unary): Result<Expression.Unary, Errors> {
        if (stack.size < 1)
            return Result.Fail(Errors.EXPECTED_NUMBER)

        val arg = stack.pop()
        if (arg !is Expression.Number)
            return Result.Fail(Errors.EXPECTED_NUMBER)

        val exp = unaryOperator.getExpression(arg)
        return Result.Ok(exp)
    }

    private fun buildBinaryOperator(binaryOperator: Token.Binary): Result<Expression.Binary, Errors> {
        if (stack.size < 2)
            return Result.Fail(Errors.EXPECTED_NUMBER)

        val secondArg = stack.pop()
        val firstArg = stack.pop()

        val exp = binaryOperator.getExpression(firstArg, secondArg)
        return Result.Ok(exp)
    }

    private fun buildResult(expressionStack: ExpStack): Result<Expression, Errors> {
        val stSize = expressionStack.size
        return when {
            stSize == 1 ->
                Result.Ok(expressionStack.pop())
            stSize > 1 ->
                Result.Fail(Errors.EXPECTED_OPERATOR)
            else ->
                Result.Fail(Errors.UNKNOWN)
        }
    }
}
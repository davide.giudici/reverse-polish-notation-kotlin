FROM adoptopenjdk/maven-openjdk11:latest AS build-phase

WORKDIR /kata
COPY ./pom.xml ./pom.xml
RUN mvn -T 2C dependency:go-offline --no-transfer-progress
COPY . .
RUN mvn package

FROM adoptopenjdk/openjdk11:jre-11.0.9_11.1-alpine
ARG APPLICATION_ARGS

WORKDIR /kata
COPY --from=build-phase /kata/target/libs /kata/libs/
COPY --from=build-phase /kata/target/reversePolishNotation.jar /kata/reversePolishNotation.jar
ENTRYPOINT ["java", "-jar", "reversePolishNotation.jar"]
